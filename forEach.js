const items = [1,2,5];
const copyItems = [];

// // before
// for (let i = 0; i < items.length; i++) {
//   copyItems.push(items[i]+2);
// }

// // after
// items.forEach((item) => {
//   copyItems.push(item);
// });

items.forEach((item)=>{
    copyItems.push(item+2)
})

console.log(copyItems)
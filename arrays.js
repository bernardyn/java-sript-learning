const myarray = [1,2,3,4,'5 tekst']
console.log('tablica przed zmianamy')
console.log(myarray)

// pop 
myarray.pop()

console.log('tablica po usunieciu ostatniego miejsca')
console.log(myarray)

//push
myarray.push('nowa wartosc')
console.log('tablica po dodaniu na koncu')
console.log(myarray)

//shift usuwa z poczatku
myarray.shift()
console.log('tablica po usunieciu z poczatku')
console.log(myarray)

//unshift dodaje na poczatku
myarray.unshift('nowa wartosc')
console.log('tablica po dodaniu na poczatku')
console.log(myarray)
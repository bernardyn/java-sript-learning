const tablica1 = [1,2,3]
const tablica2 = ['lubie placki', 4, 'kocham placki']

const nowaZlaczonaTablica = [].concat(tablica1, tablica2)

console.log(nowaZlaczonaTablica)

//zeby rozdzielic elemety specjalnym znakiem mozna polaczona tablice przetworzyc joinem

const finalTablica = nowaZlaczonaTablica.join('; ')

console.log(finalTablica)